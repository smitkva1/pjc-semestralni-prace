#pragma once

#include <string>
#include <vector>
#include <thread>
#include <iostream>
#include <fstream>
#include <exception>
#include "workspace.cpp"

class manager {
public:
	manager() = default;
	~manager() = default;

	void create_map(std::string const& file_name) {	
		std::ifstream file(file_name + ".txt");
		
		if (!file.is_open()) { throw my_excep("No file with this name in this folder."); }
		
		std::string line;
		std::getline(file, line);
		std::vector<std::string> box = split_line(line, ' ');

		input = file_name;
		citis = std::stoi(box[0]);
		roads = std::stoi(box[1]);

		check_number(citis);
		check_number(roads);
		if (unit.size() > 4) { throw my_excep("Unit is longer than expected."); }

		unit = box[2];

		inicial_map(citis, map);

		for (int i = 0; i < citis; i++) {
			map[i][i] = 0;
		}

		add_roads(file, map, roads);

		file.close();
	};

	void calculate_roads_s() {
		for (int k = 0; k < citis; k++) {
			for (int i = 0; i < citis; i++) {
				for (int j = 0; j < citis; j++) {
					if (map[i][j] > connect(map[i][k], map[k][j])) {
						map[i][j] = map[i][k] + map[k][j];
					}
				}
			}
		}
	};

	void calculate_roads_m() {
		int num_threads = std::thread::hardware_concurrency();
		if (num_threads > 1) {
			std::vector<std::thread> threads;
			int k;

			auto thread_func = [&](int i) {
				auto& mapa = map;
				while (i < citis) {
					for (int j = 0; j < citis; j++) {
						if (mapa[i][j] > connect(mapa[i][k], mapa[k][j])) {
							mapa[i][j] = mapa[i][k] + mapa[k][j];
						}
					}
					i += num_threads;
				}
			};

			for (k = 0; k < citis; k++) {
				for (int tr = 0; tr < num_threads; tr++) {
					threads.emplace_back(thread_func, tr);
				}

				for (auto& t : threads) {
					t.join();
				}

				threads.clear();
			}
		} else {
			calculate_roads_s();
		}
	};

	void print_result(std::ostream &os) {	
		int f_line_decor = 2 + citis;
		int l_line_decor = 2 * f_line_decor + 2;

		print_decoration(os, f_line_decor);
		os << "_DO_";
		print_decoration(os, f_line_decor);
		os << '\n';

		os << "< >";
		print_unit(os, unit);
		for (int i = 0; i < citis; i++) {
			print_one_box(os, i + 1);
		}
		os << "< >\n";

		std::string from;
		char decor = '=';
		int x_ord;
		for (int i = 0; i < (citis * 2); i++) {
			from = ((i + 1) == citis) ? "_Z_" : "< >";
			if (i % 2 == 0) {
				print_decor_line(os, citis, from, decor);
			} else {
				x_ord = (i / 2);
				
				os << from << ' ';  
				print_one_box(os, x_ord + 1);
				os << '|';
				
				for (int y = 0; y < citis; y++) {
					print_one_box(os, map[x_ord][y]);
				}
				os << "< >\n";
			}
			decor = '-';
		}

		print_decoration(os, l_line_decor);
		os << '\n';
	};

private:
	std::string input;
	int citis;
	int roads;
	std::string unit;

	std::vector<std::vector<int>> map;
};