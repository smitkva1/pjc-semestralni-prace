#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include <exception>
#include <limits>
#include <thread>

using map = std::vector<std::vector<int>>;

class my_excep : public std::exception {
public:
	my_excep(std::string s) : text(std::move(s)) {}
	virtual const char* what() const noexcept override {
		return text.c_str();
	}
protected:
	std::string text;
};

// Function for create a map

std::vector<std::string> split_line(std::string& line, char const delimiter) {
	std::vector<std::string> box;
	std::stringstream ss(line);
	std::string word;

	while (getline(ss, word, delimiter)) {
		box.push_back(word);
	}

	if (box.size() != 3) { throw my_excep::my_excep("First line syntax error."); }

	return box;
}

void inicial_map(int const& citis, map& mapa) {
	std::vector<int> row;
	for (int y = 0; y < citis; y++) {
		row.push_back(std::numeric_limits<int>::max());
	}
	for (int i = 0; i < citis; i++) {
		mapa.push_back(row);
	}
}

void add_roads(std::ifstream& file, map& mapa, int const& roads) {
	std::vector<std::string> box;
	std::string line;
	try {
		for (int i = 0; i < roads; i++) {
			std::getline(file, line);
			box = split_line(line, ' ');

			int const number = std::stoi(box[2]);
			if (0 <= number && number <= 999) {
				mapa[std::stoi(box[0]) - 1][std::stoi(box[1]) - 1] = number;
			}
			else {
				throw my_excep("Number is out of range on line: " + std::to_string(i+1));
			}
		}
	}
	catch (std::exception& e) {
		throw my_excep(e.what());
	}
}

void check_number(int const& num) {
	if (num <= 0 || 999 < num) {
		throw my_excep("Number is out of range");
	}
}

// Operator +

int connect(int const& a, int const& b) {
	if (a == std::numeric_limits<int>::max() ||
		b == std::numeric_limits<int>::max()) {
		return std::numeric_limits<int>::max();
	} else {
		return a + b;
	}
}

// Functions for printing

void print_decoration(std::ostream &os, int const& lenght) {
	for (int i = 0; i < lenght; i++) {
		os << "<>";
	}
}

void print_decor_line(std::ostream &os, int const& lenght, std::string const& start, char const& decor) {
	os << start << decor << decor << decor << decor << "||";
	for (int i = 0; i < lenght; i++) {
		os << decor << decor << decor << '|';
	}
	os << "< >\n";
}

void print_one_box(std::ostream &os, int const& num) {
	if (num < 10) {
		os << ' ' << num << ' ';
	}
	else if (num < 100) {
		os << ' ' << num;
	} else if (num < 1000) {
		os << num;
	} else if (num == std::numeric_limits<int>::max()) {
		os << "inf";
	}else {
		int n = num;
		do {
			n /= 10;
		} while (n < 10);
		os << n << "..";
	}
	os << '|';
}

void print_unit(std::ostream &os, std::string const& unit) {
	size_t lenght = unit.size();
	if (lenght == 1){
		os << "  " << unit << ' ';
	} else if(lenght == 2) {
		os << ' ' << unit << ' ';
	} else if(lenght == 3) {
		os << ' ' << unit;
	} else {
		os << unit;
	}
	os << "||";
}
