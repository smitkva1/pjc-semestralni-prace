#include <iostream>
#include <vector>
#include <chrono>
#include <algorithm>
#include <string>
#include <cstring>
#include "manager.hpp"

namespace timer {
	auto now() {
		return std::chrono::high_resolution_clock::now();
	}

	template <typename TimePoint>
	long long to_ms(TimePoint tp) {
		return std::chrono::duration_cast<std::chrono::milliseconds>(tp).count();
	}
}

void print_help_story(std::string const& prog_name) {
	std::clog << "Welcome to Roads Calculator!\n" <<
		"This is application using: " << prog_name << " for calculate.\n" <<
		"You can find out the shortest way from anywhere to anywhare.\n" <<
		"First step for use this calculator:\n" << 
		"	- Just create text file dot txt with this rules:\n" <<
		"		~ 1st line: <number of places>' '<number of roads>' '<unit>\n" <<
		"		~ all other lines: <from>' '<to>' '<distance>\n" <<
		"		~ take just numbers beween 0 - 999" <<
		"			+ if number is begir or smaller than return error" <<
		"		~ unit can contin maximum 4 chars" <<
		"Next step:\n" << 
		"	- Put the created file to same folder.\n" << 
		"	- Run this calculator with commands:" << 
		"		1. text file name without suffix\n" <<
		"		2. -s for one thread or -m for multitread";
}

bool is_help(std::string const& argument) {
    return argument == "--help" || argument == "-h";
}

/*
true -> multi thread
false -> single thread
error -> everything else
*/
bool second_command(std::string const& argument) {
	if (argument == "-s") {
		return false;
	} else if (argument == "-m") {
		return true;
	} else {
		throw my_excep("Second command is incorect.");
	}
}

int main(int argc, char** argv) {
    
    if (std::any_of(argv, argv+argc, is_help)) {
		print_help_story(argv[0]);
        return 0;
    }
    
	manager m;
	bool multithread = false;
    if (argc == 3) {
		try {
			m.create_map(argv[1]);
			multithread = second_command(argv[2]);
		} catch (std::exception& e) {
			std::cout << "Error: " << e.what() << std::endl;
			return 2;
		}
    } else {
        std::clog << "Error: Paremeters troubles.\n";
		print_help_story(argv[0]);
        return 1;
    }

	std::string way_of_calc;
	std::clog << "Let's calculate your troubles.\n";
    
	auto t1 = timer::now();
	if (multithread) {
		way_of_calc = "Multithread";
		m.calculate_roads_m();
	} else {
		way_of_calc = "Singlethread";
		m.calculate_roads_s();
	}
    auto t2 = timer::now();
	
	std::cout << "Result is:\n";
	m.print_result(std::cout);
    std::cout << way_of_calc << " time needed: " << timer::to_ms(t2-t1) << "ms \n";
}